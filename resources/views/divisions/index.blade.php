<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Divisions</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .delete-btn {padding: 0 !important;display: inline !important;background: transparent !important;color: var(--links) !important;}
        .delete-btn:hover {text-decoration: underline;}
        .delete-form {display: inline !important;vertical-align: middle !important;}
    </style>
</head>
<body>
    <h1>Divisions</h1>
    @can('create', \App\Models\Division::class)
        <a href="{{ route('divisions.create') }}">New Division</a> |
    @endcan
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>

        @foreach ($divisions as $division)
        <tr>
            <td>{{ $division->id }}</td>
            <td>{{ $division->name }}</td>
            <td>
                @can('viewAny', [\App\Models\Division::class, $division])
                    <a href="{{ route('users.index', ['division' => $division->id ]) }}">View</a> |
                @endcan
                @can('update', $division)
                    <a href="{{ route('divisions.edit', ['division' => $division->id ]) }}">Edit</a>
                @endcan
                @can('delete', $division)
                    | <form class="delete-form" action="{{ route('divisions.destroy', ['division' => $division->id]) }}" method='post'>
                        @csrf
                        @method('DELETE')
                        <input class="delete-btn" type="submit" value="Delete">
                    </form>
                @endcan
            </td>
        </tr>
        @endforeach

    </table>
</body>
</html>
