<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Divisions</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>New Division</h1>
    <a href="{{ route('divisions.index') }}">Divisions</a> |
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form action="{{ route('divisions.store') }}" method="POST">
        @csrf

        <label for="name">Name</label>
        <input type="text" name="name" id="name">
        <input type="submit" value="Create">
    </form>
</body>
</html>
