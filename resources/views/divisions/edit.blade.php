<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Divisions</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>Edit Division</h1>
    <a href="{{ route('divisions.index') }}">Divisions</a> |
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form action="{{ route('divisions.update', ['division' => $division->id]) }}" method="POST">
        @csrf
        @method('put')

        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $division->name }}">
        <input type="submit" value="Update">
    </form>
</body>
</html>
