<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Holidays</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .delete-btn {padding: 0 !important;display: inline !important;background: transparent !important;color: var(--links) !important;}
        .delete-btn:hover {text-decoration: underline;}
        .delete-form {display: inline !important;vertical-align: middle !important;}
    </style>

</head>
<body>
    <h1> Holiday </h1>
    <a href="{{ route('holidays.edit', ['user' => $user, 'holiday' => $holiday]) }}">Edit</a> |
    <a href="{{ route('holidays.create', $user) }}">New holiday</a>
    <hr>

    <h2> {{ $holiday->title }} </h2>
    <br>

    <p>{{ $holiday->comments }}</p>
    <br>

    <div> <strong>From : </strong> {{ $holiday->from }}</div>
    <div> <strong>To : </strong> {{ $holiday->to }}</div>
    <div> {{ $holiday->days }} days</div>
    <br>

    <div>{{ $holiday->status }}</div>
    <br>
    @if($holiday->approved === null)
        <a href="{{ route('holidays.edit', ['user' => $user, 'holiday' => $holiday]) }}">Edit</a>|
        <form class="delete-form" action="{{ route('holidays.destroy', ['user' => $user, 'holiday' => $holiday]),  }}" method='post'>
            @csrf
            @method('DELETE')
            <input class="delete-btn" type="submit" value="Delete">|
        </form>
        <form class="delete-form" action="{{ route('holidays.approve', $holiday) }}" method='post'>
            @csrf
            <input class="delete-btn" type="submit" value="Approve">|
        </form>

        <form class="delete-form" action="{{ route('holidays.reject', $holiday) }}" method='post'>
            @csrf
            <input class="delete-btn" type="submit" value="Reject">
        </form>
    @endif









</body>
</html>
