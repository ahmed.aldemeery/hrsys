<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Holidays</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .delete-btn {padding: 0 !important;display: inline !important;background: transparent !important;color: var(--links) !important;}
        .delete-btn:hover {text-decoration: underline;}
        .delete-form {display: inline !important;vertical-align: middle !important;}
    </style>
</head>
<body>
    <h1>{{ $user->name }} holidays</h1>
    @can('create', [\App\Models\Holiday::class, $user])
        <a href="{{ route('holidays.create', $user) }}">New Holiday</a> |
    @endcan
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <table>
        <tr>
            <th style="width: 70px">ID</th>
            <th style="width: 90px">Title</th>
            <th style="width: 90px">Type</th>
            <th style="width: 90px">Days</th>
            <th style="width: 95px">Status</th>
            <th>Actions</th>
        </tr>

        @foreach ($holidays as $holiday)
        <tr>
            <td>{{ $holiday->id }}</td>
            <td>{{ $holiday->title }}</td>
            <td>{{ $holiday->type }}</td>
            <td>{{ $holiday->days }}</td>
            <td>{{ $holiday->status }}</td>
            <td>
                <a href="{{ route('holidays.show', ['user' => $user,'holiday' => $holiday]) }}">View</a>

                @if($holiday->approved === null)
                    @can('update', $holiday)
                    | <a href="{{ route('holidays.edit', ['user' => $user, 'holiday' => $holiday]) }}">Edit</a>
                    @endcan
                    @can('delete', $holiday)
                        | <form class="delete-form" action="{{ route('holidays.destroy', ['user' => $user, 'holiday' => $holiday]),  }}" method='post'>
                            @csrf
                            @method('DELETE')
                            <input class="delete-btn" type="submit" value="Delete">|
                        </form>
                    @endcan
                    @can('approve', $holiday)
                        <form class="delete-form" action="{{ route('holidays.approve', $holiday) }}" method='post'>
                            @csrf
                            <input class="delete-btn" type="submit" value="Approve">|
                        </form>
                    @endcan
                    @can('reject', $holiday)
                        <form class="delete-form" action="{{ route('holidays.reject', $holiday) }}" method='post'>
                            @csrf
                            <input class="delete-btn" type="submit" value="Reject">
                        </form>
                    @endcan
                @endif
            </td>
        </tr>
        @endforeach


    </table>
</body>
</html>
