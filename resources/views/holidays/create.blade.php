<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Holidays</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>New Holiday</h1>
    <hr>
    <form action="{{ route('holidays.store', $user) }}" method="POST">
        @csrf

        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="{{ old('title') }}">

        <label for="comments">Comments</label>
        <textarea name="comments" id="comments" cols="30" rows="10">{{ old('comments') }}</textarea>

        <label for="from">From</label>
        <input type="date" name="from" id="from" value="{{ old('from') }}">

        <label for="to">To</label>
        <input type="date" name="to" id="to" value="{{ old('to') }}">

        <label for="type">Sick</label>
        <input type="radio" name="type" id="type" value="sick">

        <label for="type">Casual</label>
        <input type="radio" name="type" id="type" value="casual">

        <input type="submit" value="Create">
    </form>
</body>
</html>
