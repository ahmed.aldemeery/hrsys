<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Holidays</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>Updata Holiday</h1>
    <hr>
    <form action="{{ route('holidays.update', ['user' => $user, 'holiday' => $holiday]) }}" method="POST">
        @csrf

        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="{{ $holiday->title }}">

        <label for="comments">comments</label>
        <textarea name="comments" id="comments"  cols="30" rows="10" >{{ $holiday->comments }}</textarea>

        <label for="from">From</label>
        <input type="date" name="from" id="from" value="{{ $holiday->from->format('Y-m-d') }}">

        <label for="to">To</label>
        <input type="date" name="to" id="to" value="{{ $holiday->to->format('Y-m-d') }}">

        <label for="sick">Sick</label>
        <input type="radio" name="type" id="sick" value="sick" {{ $holiday->type == 'sick' ? 'checked' : '' }}>
        <label for="casual">Casual</label>
        <input type="radio" name="type" id="casual" value="casual" {{ $holiday->type == 'casual' ? 'checked' : '' }}>

        <input type="submit" value="Update">

    </form>
</body>
</html>
