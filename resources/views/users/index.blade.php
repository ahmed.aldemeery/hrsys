<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .delete-btn {padding: 0 !important;display: inline !important;background: transparent !important;color: var(--links) !important;}
        .delete-btn:hover {text-decoration: underline;}
        .delete-form {display: inline !important;vertical-align: middle !important;}
    </style>
</head>
<body>
    <h1> {{ $division->name }} users</h1>
    @can('create', [\App\Models\User::class, $division])
    <a href="{{route('users.create', $division) }}">New User</a> |
    @endcan
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Admin</th>
            <th>Actions</th>
        </tr>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->is_admin ? '✅' : '❌' }}</td>
            <td>
                <a href="{{ route('holidays.index', ['user' => $user->id]) }}">View</a> |
                @can('update', $user)
                    <a href="{{ route('users.edit', ['division' => $division->id, 'user' => $user->id ]) }}">Edit</a>|
                @endcan
                @can('delete', $user)
                    <form class="delete-form" action="{{ route('users.destroy', ['division' => $division->id, 'user' => $user->id ]) }}" method='post'>
                    @csrf
                    @method('DELETE')
                    <input class="delete-btn" type="submit" value="Delete">
                @endcan

                </form>
            </td>

        </tr>
        @endforeach

    </table>
</body>
</html>
