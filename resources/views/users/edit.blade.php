<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>Update User</h1>
    <a href="{{ route('users.index', $division) }}">Users</a> |
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <form action="{{ route('users.update', ['division' => $division, 'user' => $user]) }}" method="POST">
        @csrf

        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $user->name }}">

        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="{{ $user->email }}">

        <label for="password">Password</label>
        <input type="password" name="password" id="password">

        <label for="password_confirmation">Confirm Password</label>
        <input type="password" name="password_confirmation" id="password_confirmation">

        <label for="admin">Admin</label>
        <input type="checkbox" name="is_admin" id="admin" {{ $user->is_admin ? "checked" : ""}}  >

        <input type="submit" value="Update">
    </form>
</body>
</html>
