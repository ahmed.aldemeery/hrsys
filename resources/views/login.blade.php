<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">

</head>
<body>
    <h1>Login</h1>
    <hr>
    <form action="{{ route('signin') }}" method="post">
        @csrf

        @if(session('message'))
            <p style="color: red;">{{ session('message') }}</p>
        @endif

        <label for="email">Email</label>
        <input type="text" name="email" value='{{ old('email') }}' id="email">

        <label for="password">Password</label>
        <input type="password" name="password" id="password">

        <br>

        <input type="submit" value="Login">
    </form>
</body>
</html>
