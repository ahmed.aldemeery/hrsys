<?php

namespace App\Policies;

use App\Models\Division;
use App\Models\Holiday;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HolidayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        $division = $user->division;

        return ($user->isSuperAdmin()) || ($user->isAdmin() && $user->division_id == $division->id);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Holiday $holiday)
    {
        $division = $holiday->user->division;

        return ($user->isSuperAdmin()) || ($user->isAdmin() && $user->division_id == $division->id) || ($holiday->user_id == $user->id);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $authenticatedUser, User $user)
    {
       return $authenticatedUser->is($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Holiday $holiday)
    {
        return $holiday->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Holiday $holiday)
    {
        return $holiday->sameUser($user) && $holiday->approved === null;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function approve(User $user, Holiday $holiday)
    {
        $division = $holiday->user->division;

        return ($user->isSuperAdmin()) || ($user->isAdmin() && $user->division_id == $division->id && $holiday->approved == null);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function reject(User $user, Holiday $holiday)
    {
        $division = $holiday->user->division;

        return ($user->isSuperAdmin()) || ($user->isAdmin() && $user->division_id == $division->id && $holiday->approved == null);

    }
}
