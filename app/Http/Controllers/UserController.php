<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Division;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(Division $division)

    {
        $this->authorize('viewAny', [User::class, $division]);

        $users = $division->users()->get();

        return view('users/index', [
            'division' => $division,
            'users' => $users
        ]);
    }

    public function create(Division $division)
    {
        $this->authorize('create', [User::class, $division]);

        return view('users/create', [
            'division' => $division
        ]);

    }

    public function store(Request $request, Division $division)
    {
        $this->authorize('create', [User::class, $division]);

        $data = $request->validate([
            'division_id' => 'exists:divisions,id',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        if ($request->has('is_admin')) {
            $data['is_admin'] = true;
        }

        $data['password'] = bcrypt($data['password']);

        $division->users()->create($data);

        return to_route('users.index', [
            'division' => $division,
        ]);


    }

    public function destroy(Division $division, User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();

        return to_route('users.index',[
            'division' => $division
        ]);
    }
    public function edit(Division $division, User $user)
    {
        $this->authorize('update', $user);
        return view('users/edit', [
            'division' => $division,
            'user' => $user
        ]);

    }

    public function update(Request $request, Division $division, User $user)
    {
        $this->authorize('update', $user);
        $data = array_filter($request->validate([
            'name' => 'required|string',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id)
            ],
            'password' => 'confirmed',
        ]));

        if ($request->has('is_admin')) {
            $data['is_admin'] = true;
        } else {
            $data['is_admin'] = false;
        }

        $user->update($data);

        return to_route('users.index',[
            'division' => $division,
            'user' => $user
        ]);
    }


}
