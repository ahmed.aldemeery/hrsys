<?php

namespace App\Http\Controllers;

use App\Models\Division;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        if ($user->division->is(Division::first())){
            return to_route('divisions.index');
        } elseif($user->is_admin == 1)  {
            return to_route('users.index', $user->division);
        } else {
            return to_route('holidays.index', $user);
        }
    }
}
