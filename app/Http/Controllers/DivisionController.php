<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Division;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DivisionController extends Controller
{

    public function index()
    {
        $this->authorize('viewAny', Division::class);

        return view('divisions/index', [
            'divisions' => Division::get(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', Division::class);

        return view('divisions/create');
    }

    public function store(Request $request)
    {
        $this->authorize('create', Division::class);

        $data = $request->validate([
            'name' => 'required|string',
        ]);

        Division::create($data);

        return to_route('divisions.index');
    }

    public function edit(Division $division)
    {
        $this->authorize('update', $division);

        return view('divisions/edit', [
            'division' => $division
        ]);
    }

    public function update(Request $request, Division $division)
    {
        $this->authorize('update', $division);

        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $division->update($data);

        return to_route('divisions.index');
    }

    public function destroy(Division $division)
    {
        $this->authorize('delete', $division);

        $division->delete();

        return to_route('divisions.index');
    }
}
