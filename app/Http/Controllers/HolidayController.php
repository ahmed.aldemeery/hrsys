<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Division;
use App\Models\Holiday;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class HolidayController extends Controller
{
    public function index(User $user)
    {
        $holidays = $user->holidays()->get();

        return view('holidays/index', [
            'user' => $user,
            'holidays' => $holidays
        ]);
    }

    public function reject(Holiday $holiday)
    {
        $this->authorize('reject', $holiday);

        $holiday->approved = false;
        $holiday->save();

        return back();
    }

    public function approve(Holiday $holiday)
    {
        $this->authorize('approve', $holiday);

        $holiday->approved = true;
        $holiday->save();

        return back();
    }

    public function create(User $user)
    {
        $this->authorize('create', [Holiday::class, $user]);

        return view('holidays/create', [
            'user' => $user
        ]);
    }

    public function store(Request $request, User $user)
    {
        $this->authorize('create', [Holiday::class, $user]);

        $data = $request->validate([
            'title' => 'required|string',
            'comments' => 'string',
            'type' => Rule::in(['sick', 'casual']),
            'from' => 'required|date|after:yesterday',
            'to' => 'required|date|after:from',
        ]);

        $user->holidays()->create($data);

        return to_route('holidays.index', [
            'user' => $user,
        ]);


    }

    public function edit(User $user, Holiday $holiday)
    {
        $this->authorize('update', $holiday);

        return view('holidays/edit', [
            'user' => $user,
            'holiday' => $holiday
        ]);
    }

    public function update(Request $request, User $user, Holiday $holiday)
    {
        $this->authorize('update', $holiday);

        $data = $request->validate([
            'title' => 'required|string',
            'comments' => 'string',
            'type' => Rule::in(['sick', 'casual']),
            'from' => 'required|date|after:yesterday',
            'to' => 'required|date|after:from',
        ]);

        $user->holidays()->update($data);

        return to_route('holidays.index', [
            'user' => $user,
            'holiday' => $holiday
        ]);
    }

    public function show(User $user, Holiday $holiday)
    {
        $this->authorize('view', $holiday);

        return view('holidays/view',[
            'user' => $user,
            'holiday' => $holiday,
        ]);
    }

    public function destroy(User $user, Holiday $holiday)
    {
        $this->authorize('delete'. $holiday);

        $holiday->delete();

        return to_route('holidays.index',[
            'holiday' => $holiday,
            'user' => $user
        ]);
    }
}
