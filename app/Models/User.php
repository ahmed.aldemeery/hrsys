<?php

namespace App\Models;

use App\Models\Holiday;
use App\Models\Division;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */


    public function division(): BelongsTo
    {
        return $this->belongsTo(Division::class);
    }


    public function holidays(): HasMany
    {
        return $this->hasMany(Holiday::class);
    }

    public function isSuperAdmin(): bool
    {
        return $this->division->is(Division::first());
    }

    public function isInDivision(Division $division): bool
    {
        return $this->division_id !== $division->id;
    }

    public function isAdmin(): bool
    {
        return $this->is_admin == true;
    }

    public function sameUser(User $user): bool
    {
        return $this->id == $user->id;
    }
}
