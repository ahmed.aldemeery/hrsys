<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Holiday extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'comments',
        'type',
        'from',
        'to',
        'approved',
    ];

    protected $casts = [
        'from' => 'datetime',
        'to' => 'datetime',
        'approved' => 'boolean',
    ];


    public function user(): BelongsTo {
        return $this->belongsTo((User::class));
    }

    protected function days(): Attribute
    {
        return Attribute::make(
            fn () => $this->from->diffInDays($this->to),
        );
    }

    protected function status(): Attribute
    {

        return Attribute::make(
            get: fn () => $this->approved ? 'Approved ✅' : ($this->approved === null ? 'Pending...' : 'Rejected ❌')
        );
    }

    public function sameUser(User $user): bool
    {
        return $this->id == $user->id;
    }

}
