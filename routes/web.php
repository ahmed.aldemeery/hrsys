<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\DivisionController;


Route::group(['middleware' => 'auth'], function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');

    // Logout
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    // Divisions
    Route::resource('divisions', DivisionController::class);

    //Users
    Route::get('divisions/{division}/users', [Usercontroller::class, 'index'])->name('users.index');
    Route::get('divisions/{division}/users/create', [UserController::class, 'create'])->name('users.create');
    Route::post('divisions/{division}/users', [UserController::class, 'store'])->name('users.store');
    Route::delete('divisions/{division}/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
    Route::get('divisions/{division}/users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');
    Route::post('divisions/{division}/users/{user}', [UserController::class, 'update'])->name('users.update');

    //Holidays
    Route::get('users/{user}/holidays', [HolidayController::class, 'index'])->name('holidays.index');
    Route::post('holidays/{holiday}/reject', [HolidayController::class, 'reject'])->name('holidays.reject');
    Route::post('holidays/{holiday}/approve', [HolidayController::class, 'approve'])->name('holidays.approve');
    Route::get('users/{user}/holidays/create', [HolidayController::class, 'create'])->name('holidays.create');
    Route::post('users/{user}/holidays', [HolidayController::class, 'store'])->name('holidays.store');
    Route::get('users/{user}/holidays/{holiday}/edit', [HolidayController::class, 'edit'])->name('holidays.edit');
    Route::post('users/{user}/holidays/{holiday}', [HolidayController::class, 'update'])->name('holidays.update');
    Route::get('users/{user}/holidays/{holiday}', [HolidayController::class, 'show'])->name('holidays.show');
    Route::delete('users/{user}/holidays/{holiday}', [HolidayController::class, 'destroy'])->name('holidays.destroy');
});



Route::group(['middleware' => 'guest'], function () {
    Route::get('/', [AuthController::class, 'login'])->name('login');
    Route::post('signin', [AuthController::class, 'signin'])->name('signin');
});

