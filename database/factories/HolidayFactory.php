<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Holidays>
 */
class HolidayFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'comments' => $this->faker->paragraph(10),
            'type' => $this->faker->randomElement(['sick', 'casual']),
            'from' => $from = new Carbon($this->faker->dateTimeThisYear()),
            'to' => $from->copy()->addDay($this->faker->numberBetween(2, 7)),
            'approved' =>  $this->faker->randomElement([true, false, null]),
        ];
    }
}
